<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/action',
        __DIR__ . '/base',
        __DIR__ . '/formulaires',
        __DIR__ . '/inc',
        __DIR__ . '/prive',
        __DIR__ . '/puce_statut',
		__DIR__ . '/mots_administration.php',
		__DIR__ . '/mots_autoriser.php',
		__DIR__ . '/mots_ieconfig.php',
		__DIR__ . '/mots_pipelines.php',
    ]);

	$rectorConfig->sets([
		LevelSetList::UP_TO_PHP_81
	]);
};
